# Horkos Example

This repo is an example of `horkos-doc` can be used to document a data set.

To view the rendered documentation go to: https://kjschiroo.gitlab.io/horkos-example/
